const { gql } = require('apollo-server-koa')

const typeDefs = gql`
    type Article {
        key: Int!                        # "!" denotes a required field
        title: String
    }

    type User {
        key: Int!                         # "!" denotes a required field
        title: String
    }

    # This type specifies the entry points into our API. 
    type Query {
        articles: [Article]             # "[]" means this is a list of channels
        article(id: Int!): Article
        users: [User]
        user(id: Int!): User
    }
    `

module.exports = typeDefs
