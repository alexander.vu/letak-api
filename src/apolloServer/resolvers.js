
/**
 * /
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const articles = require('../models/articles.js')
const users = require('../models/users.js')

const resolvers = {
  Query: {
    articles: () => articles,
    article: (root, { id }) => articles.find(article => id === article.key),
    users: () => users,
    user: (root, { id }) => users.find(user => id === user.key)
  }
}
module.exports = resolvers
