/**
 * GraphQL middleware
 * Init GraphQL, set routes and load GraphQL Schemes.
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const config = require('config')
const { ApolloServer } = require('apollo-server-koa')
const typeDefs = require('./typedefs.js')
const resolvers = require('./resolvers.js')
const apollo = new ApolloServer({ typeDefs, resolvers })

function init (koaApp) {
  if (config.has('settings.graphql') !== false && config.get('settings.graphql') !== false) {
    apollo.applyMiddleware({ app: koaApp })
  }
}

module.exports = init
