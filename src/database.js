/**
 * Database init
 *
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const config = require('config')
const mongoose = require('mongoose')
const logger = require('./logger')

async function init () {
  if (config.has('settings.database') !== false &&
      config.get('settings.database') !== false) {
    const settings = config.get('settings.database')

    await mongoose.connect(settings.uri, settings.options || { useNewUrlParser: true }).then(
      () => {
        /** Ready to use. The `mongoose.connect()` promise resolves to undefined. */
        logger.info(`Connected to mongo db ${settings.uri}`)
      },
      (error) => {
        logger.error(error)
      }
    )
  }
}

module.exports = init
