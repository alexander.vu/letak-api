/**
 * Compose Middleware for API server
 * TODO Document cors settings!
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const path = require('path')
const compose = require('koa-compose')
const mount = require('koa-mount')
const staticFiles = require('koa-static')
const helmet = require('koa-helmet')
const bodyparser = require('koa-bodyparser')
const resposetime = require('koa-response-time')
const cors = require('@koa/cors')
const errorHandler = require('./error.js')
const config = require('config')

const middlewareList = [bodyparser()]

// Error middleware
if (config.has('settings.error') !== false &&
    config.get('settings.error') !== false) {
  middlewareList.push(errorHandler())
}

// ResponseTime middleware
if (config.has('settings.resposetime') !== false &&
    config.get('settings.resposetime') !== false) {
  middlewareList.push(resposetime())
}

// Cors middleware
if (config.has('settings.cors') !== false &&
    config.get('settings.cors') !== false) {
  const corsSettings = typeof config.get('settings.cors') === 'object' ? config.get('settings.cors') : {}
  middlewareList.push(cors(corsSettings))
}

// Helmet middleware
if (config.has('settings.helmet') !== false &&
    config.get('settings.helmet') !== false) {
  const helmetSettings = typeof config.get('settings.helmet') === 'object' ? config.get('settings.helmet') : {}
  middlewareList.push(helmet(helmetSettings))
}

// static files
if ((config.has('settings.staticFiles') !== false &&
     config.get('settings.staticFiles') !== false) &&
    config.get('settings.staticFiles.mount') &&
    config.get('settings.staticFiles.path')) {
  middlewareList.push(
    mount(config.get('settings.staticFiles.mount'),
      staticFiles(path.join(__dirname, config.get('settings.staticFiles.path')), config.get('settings.staticFiles.options') || {}))
  )
}

module.exports = () => compose(middlewareList)
