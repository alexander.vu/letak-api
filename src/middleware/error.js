/**
 * Compose error handling as middleware for API server
 *
 * Handle all errors called by ctx.throw([status], [msg], [properties]) or
 * ctx.assert(value, [status], [msg], [properties]).
 *
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
function errorMiddlewareComposer () {
  return async function errorMiddleware (ctx, next) {
    try {
      await next()
    } catch (err) {
      ctx.status = err.status || 500
      ctx.body = err.message

      ctx.log.error({
        message: err.message
      })
    }
  }
}

module.exports = errorMiddlewareComposer
