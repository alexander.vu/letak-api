/**
 * CREM Mongoose Model Accounts
 *
 * @author    Alexander Vu
 * @copyright Alexander Vu
 * @license   private
 */
const mongoose = require('mongoose')

mongoose.set('useCreateIndex', true)

const Items = new mongoose.Schema({
  origin: {
    type: String
  },
  ident: {
    type: String,
    index: true
  },
  dateFrom: {
    type: Date
  },
  dateTo: {
    type: Date
  },
  title: {
    type: String
  },
  image: {
    type: String
  },
  price: {
    type: Number
  },
  baseprice: {
    type: String
  },
  amount: {
    type: String
  },
  link: {
    type: String
  },
  originLink: {
    type: String
  }
}, {
  timestamps: {
    createdAt: 'created_at'
  }
})

module.exports = mongoose.model('Items', Items)
