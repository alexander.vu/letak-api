/**
 * Put some description here
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const Koa = require('koa')
const logger = require('./logger')
const config = require('config')
const middleware = require('./middleware')
const apolloServer = require('./apolloServer')
const composedRoutes = require('./routes')
const connectMongo = require('./database')
const packageConfig = require('../package.json')

module.exports = () => {
  const serverApp = new Koa()

  serverApp.context.version = `${packageConfig.version}#${packageConfig.build}`
  serverApp.context.config = config
  serverApp.context.log = logger

  /*
   * Setup middleware
   */
  connectMongo()
  serverApp.use(middleware())
  serverApp.use(composedRoutes())
  apolloServer(serverApp)

  return serverApp
}
