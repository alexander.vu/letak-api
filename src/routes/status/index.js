/**
 * Have to put some description here
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const Router = require('koa-router')
const setStatusRoutes = require('./status')

/* Init Router and settings */
const statusRouter = new Router({
  prefix: '/status'
})

/* Inject routes */
setStatusRoutes(statusRouter)

module.exports = statusRouter
