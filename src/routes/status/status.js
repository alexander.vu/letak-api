/**
 * Router will output a JSON object with the current API version.
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
module.exports = (router) => {
  router
    .get('/', async (ctx, next) => {
      ctx.body = { version: 1 }
      ctx.status = 200
      await next()
    })
}
