/**
 * Article router
 * Router will output a JSON object with the current API version.
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const Items = require('../../models/items.js')

module.exports = (router) => {
  router
    .post('/items', async (ctx, next) => {
      const query = {}

      if (ctx.request.body.startdate) {
        query.$or = [{ weekyear: { '$eq': ctx.request.body.startdate } }, { weekyear: { '$gte': ctx.request.body.startdate } }]
      }

      if (ctx.request.body.filter) {
        const likeQuery = { $regex: `.*${ctx.request.body.filter}.*`, $options: 'i' }
        query.$or = [
          { title: likeQuery },
          { baseprice: likeQuery },
          { amount: likeQuery }
        ]
      }
      console.log(query)
      ctx.body = await Items.find(query)
    })
    .get('/item/:ident', async (ctx, next) => {
      ctx.body = await Items.find({ ident: ctx.params.ident })
    })
}
