/**
 * User router
 * Router will output a JSON object with the current API version.
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const users = require('../../models/users.js')

module.exports = (router) => {
  router
    .get('/users', async (ctx, next) => {
      ctx.body = users
      ctx.status = 200
      await next()
    })
    .get('/user/:id', async (ctx, next) => {
      ctx.body = users.find(user => user.key === parseInt(ctx.params.id, 0))
      ctx.status = 200
      await next()
    })
}
