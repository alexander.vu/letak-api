/**
 * Put some description here
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const Router = require('koa-router')
const setUserRoutes = require('./user')
const setArticleRoutes = require('./article')
const setItemRoutes = require('./item')

/* Init Router and settings */
const restRouter = new Router({
  prefix: '/api'
})

/* Inject routes */
setUserRoutes(restRouter)
setArticleRoutes(restRouter)
setItemRoutes(restRouter)

/* export Router */
module.exports = restRouter
