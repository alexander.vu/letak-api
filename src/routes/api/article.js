/**
 * Article router
 * Router will output a JSON object with the current API version.
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const articles = require('../../models/articles.js')

module.exports = (router) => {
  router
    .get('/articles', async (ctx, next) => {
      ctx.body = articles
      ctx.status = 200
      await next()
    })
    .get('/article/:id', async (ctx, next) => {
      ctx.body = articles.find(article => article.key === parseInt(ctx.params.id, 0))
      ctx.status = 200
      await next()
    })
}
