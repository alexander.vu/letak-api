/**
 * Static Route Loading
 * Routes are loaded static here. Each Router must be required and will be composed to gether.
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const combineRouters = require('koa-combine-routers')
const statusRoutes = require('./status')
const apiRoutes = require('./api')

const composedRoutes = combineRouters([
  statusRoutes,
  apiRoutes
])

module.exports = composedRoutes
