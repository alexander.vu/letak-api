/**
 * Put some description here
 * @author    Alexander Vu <alexander.vu.tuyet@gmail.com>
 * @license   GPL-3.0
 */
const config = require('config')
const http = require('http')
const internalIP = require('internal-ip')
const logger = require('./src/logger')
const ServerApp = require('./src/serverApp')
const serverApp = ServerApp()
const httpServer = http.createServer(serverApp.callback())

/**
 * Output when server is up and listening.
 */
async function isListening () {
  if (process.env.NODE_ENV !== 'production') {
    console.log('\n  App running at:')
    console.log('  - Local:   \x1b[36m%s\x1b[0m', `http://localhost:${config.get('http.port')}/`)
    console.log('  - Network: \x1b[36m%s\x1b[0m', `http://${internalIP.v4.sync()}:${config.get('http.port')}/`)
    console.log('\n  Note that the development build is not optimized.\n')
  } else {
    logger.info(`Server is up on port ${config.get('http.port')}.`)
  }
}

httpServer.listen(process.env.PORT || config.get('http.port'), isListening)
